---
title: Algemene uitgangspunten
weight: 1
---

## Algemene uitgangspunten

Voor de berekening van de indicatoren en informatievragen in de verschillende uitwisselprofielen worden algemene uitgangspunten gehanteerd. Uitgangspunten die gelden voor specifieke indicatoren of informatievragen worden bij de functionele beschrijving van de betreffende indicator beschreven. Indicator-specifieke uitgangspunten gaan voor op algemene uitgangspunten van een uitwisselprofiel. 

Voor alle uitwisselprofielen gelden onderstaande algemene uitgangspunten.

Afspraken over de verversingstermijnen staan hier: https://kik-v-publicatieplatform.nl/afsprakenset/_/content/docs/modelgegevensset


### Uren omrekenen naar fte

Voor een aantal indicatoren en informatievragen waar het aantal fte berekend wordt geldt het volgende. Bij deze indicatoren worden het aantal uren berekend. Bij een aantal indicatoren wordt in een laatste stap ook het aantal fte (o.b.v. 36 uren) berekend. Daarnaast kan iedere afnemer van gegevens op basis van het aantal berekende uren zijn/haar eigen definitie van fte toepassen, zoals 36, 38 of 40 uur per week. Een afnemer kan op deze manier ook zelf het aantal fte berekenen.


### Relaties tussen werkovereenkomsten, rollen en groepen

Voor de berekeningen met werkovereenkomsten en/of medewerkers worden de volgende definities en relaties gehanteerd: [klik hier](purl.org/ozo/onz-g).


### Zorgverleners selecteren

Elke zorgaanbieder bepaalt op basis van de 'functie', welke personen vallen onder zorggerelateerde werknemers.  
Hierbij wordt de volgende werkwijze gehanteerd:

* De zorgaanbieder bepaalt welke functies zorgverlener zijn (de overige functies behoren dan tot de groep ‘niet-zorgverlener’).
* Of een persoon een zorgverlener is wordt bepaald op basis van de functie van die persoon gedurende een bepaalde periode.
* Dit betreft de functie die vermeld staat in de werkovereenkomst van de werknemer.
* Functies die bijvoorbeeld kunnen vallen onder de definitie van zorgverlener zijn: behandelaren, verpleegkundigen, verzorgenden, helpenden, geestelijk verzorgenden, gastvrouwen, vrijwilligerscoördinatoren, activiteitencoördinatoren, welzijnsmedewerkers, medewerkers activiteitenbegeleiding, beweegagogen, sociaal agogen, leerlingen, medewerkers leefplezier, woonbegeleiders, medewerkers zorg & welzijn, zij instromers met BBL-opleiding, stagiaires, huiskamermedewerkers, SPW-ers, familiecoaches, voedingsassistenten die direct werken met klanten, huishoudelijke medewerkers of facilitaire medewerkers die direct werken met klanten, catering medewerkers die direct werken met klanten, locatiemanagers en teamleiders en anderen als ze (deels) werken als zorgpersoneel.
* Nota bene: De aanwezigheid en/of hoeveelheid direct en indirect cliënt-contact is niet bepalend voor de selectie van functies. Of en in hoeverre de aanwezigheid en/of hoeveelheid cliënt-contact wordt meegewogen in de selectie van functies die als zorgverlener worden aangemerkt, is aan de zorgaanbieder zelf.

NB: De zorgaanbieder dient hiervoor een lijst met de indeling van de functies op te stellen (en te onderhouden). Bijvoorbeeld:

| Functie                        | Zorgverlener? |
|--------------------------------|---------------|
| Specialist ouderen geneeskunde | Ja            |
| Functioneel beheerder          | Nee           |


### Kwalificatieniveau bepalen

Elke zorgaanbieder bepaalt welke personen vallen in een bepaald kwalificatieniveau. Hierbij wordt de volgende werkwijze gehanteerd:  

* Het kwalificatieniveau wordt bepaald op basis van de functie die geregistreerd is bij de werkovereenkomst.
* Elke functie is ingedeeld in maximaal één kwalificatieniveau.
* Een kwalificatieniveau kan meerdere functies bevatten.
* De zorgaanbieder bepaalt in welk kwalificatieniveau een functie wordt ingedeeld. De indeling vindt plaats in een van de 9 kwalificatieniveaus uit de lijst met kwalificatieniveaus uit het Handboek in het kwaliteitskader verpleeghuiszorg.
* Doorstroom wordt bepaald o.b.v. het kwalificatieniveau o.b.v. de functie in de werkovereenkomst op 2 verschillende tijdstippen. Wanneer op een van deze tijdstippen het kwalificatieniveau niet bepaald kan worden, worden de betreffende werkovereenkomsten niet meegenomen in de berekening.

De indeling vindt plaats in een van de volgende 9 kwalificatieniveaus:

* Kwalificatieniveau 1;
* Kwalificatieniveau 2;
* Kwalificatieniveau 3;
* Kwalificatieniveau 4;
* Kwalificatieniveau 5;
* Kwalificatieniveau 6;
* Behandelaren/(para)medisch;
* Overig zorgpersoneel;
* Leerlingen.

De indeling van zorgverleners in een kwalificatieniveau wordt bepaald op basis van de functie in de werkovereenkomst. Om te bepalen in welk kwalificatieniveau een functie ingedeeld dient te worden, kan het volgende overzicht als leidraad gebruikt worden:

* Kwalificatieniveau 1: zorghulp/ zorg-assistent;
* Kwalificatieniveau 2: helpende (inclusief 2+: helpende met extra certificaat om medicatie te mogen delen);
* Kwalificatieniveau 3: verzorgende (IG);
* Kwalificatieniveau 4: verpleegkundige op mbo-niveau;
* Kwalificatieniveau 5: verpleegkundige op hbo-niveau;
* Kwalificatieniveau 6: verpleegkundige op hbo-niveau;
* Behandelaren/(para)medisch: SOG;
* Overig zorgpersoneel: ...;
* Leerlingen: BBL-leerling.


### Ziekteperiode indelen in kort of lang
* Een verzuimgeval wordt ingedeeld in één duurklasse. Een duurklasse is 'kort' of 'lang'. Kort betreft een periode van 28 dagen, lang een periode van 29 dagen of langer.
* De totale duur van het verzuim is bepalend voor de indeling in duurklassen 'kort' en 'lang'. De duur van het verzuim wordt bepaald op basis van de eerste ziektedag en de laatste ziektedag (hersteldatum minus 1 dag) of, in geval de laatste ziektedag nog niet is bereikt, tot en met de einddatum van de verslagperiode. De eerste ziektedag kan daarbij dus voor de verslagperiode (van bijvoorbeeld een kwartaal) liggen. Stel dat een werknemer 405 dagen ziek is geweest. In dat geval worden alle ziektedagen in de duurklasse 'lang' geplaatst. Als we kijken naar de verslagperiode (van bijvoorbeeld een kwartaal) waarin de werknemer op dag 11 hersteld was, dan vallen de eerste 395 dagen voor de verslagperiode (van het betreffende kwartaal) en de overige 10 ziektedagen in de verslagperiode (het betreffende kwartaal). Het aantal ziektedagen van 10 tellen mee bij 'lang' verzuim in de verslagperiode.

### Verzuimpercentage
* Voor de berekening van verzuim wordt gewerkt met kalenderdagen (i.p.v. werkdagen/dienstverbanddagen). De berekening van verzuim is gebaseerd op de CBS-methode (kalenderdagen), in combinatie met de part-time factor. Bij Vernet wordt tevens gebruik gemaakt van part-time factor. 
* Waarom kalenderdagen? Je hoeft geen rekening te houden met wanneer iemand ‘zou moeten werken’. Anders is er noodzaak naar de registratie van beoogde te werken uren te gaan kijken (planning). Deze gegevens  zijn ons inziens lastiger uit bronsystemen te halen.
* Waarom parttime-time factor? Zuiverdere berekening van capaciteitsverlies in geval van parttime werkers. 


### Indeling in Wlz-sectoren

Bij een aantal indicatoren worden cliënten op basis van de Wlz-indicatie ingedeeld in een van de volgende 8 Wlz-sector: 
* VV; 
* VG; 
* LVG; 
* LG; 
* ZGaud; 
* ZGvis; 
* GGZ-B
* GGZ-Wonen.

De zorgprofielen die binnen elk van deze sectoren vallen staan op de volgende website gepubliceerd: https://tog.vektis.nl/Webinfo.aspx?ID=Prestatiecodelijsten Ten aanzien van de bovenstaande 8 Wlz-sectoren betreft dit de volgende lijst: 055 - Prestatiecodelijst Wlz. 


### Gebruik van vestiging

Bij een aantal indicatoren worden de resultaten berekend per _vestiging_. De zorgkantoren en de IGJ spreken hier ook wel over _locatie_. In de model gegevensset wordt hiervoor vestiging gebruikt zoals deze is geregistreerd bij de KvK.


### Het gebruik van declaraties

Bij een aantal indicatoren worden gegevens uit de indicatie gebruikt voor:
- het indelen van declaraties en cliënten in een wet, Wlz-sector of leveringsvorm in de VV (Verblijf, VPT, MPT en PGB). 
- het berekenen van bedragen die gedeclareerd zijn.
- het bepalen van de periode waarin de zorg geleverd is.

De berekening van deze indicatoren en verdeelsleutels vindt plaats op basis van gegevens de codelijsten zoals gepubliceerd door Vektis en het Zorginstituut Nederland.   

_NB: Voor het berekenen van de indicatoren wordt gebruikt gemaakt van de gegevens zoals deze (op basis van de indicatie) zijn verwerkt in het ECD. Welke velden dit in een bepaald ECD betreft, staat beschreven in het referentieontwerp van het betreffende ECD. Hierin staat tevens beschreven op welke manier deze gegevens ontsloten kunnen worden vanuit het ECD en gerelateerd zijn aan de ontologie. Daarnaast kan het voorkomen dat er extra gegevens nodig zijn om gegevens aan elkaar te relateren, bijvoorbeeld een identificatie. Ook deze gegevens staan vermeld in het referentieontwerp

Daarnaast dient de verwerking van deze gegevens tot indicatoren nog in de praktijk getoetst te worden. Onduidelijk is bijvoorbeeld nog op welke manieren een declaratie als goedgekeurd wordt aangemerkt._

#### Indelen in Zvw, Wlz, sector VV in de Wlz, ELV en GRZ

Een indeling in de **Wlz** vindt plaats als de prestatiecode bij de indicatie voorkomt op de volgende codelijst:
- '055 - Prestatiecodelijst Wlz'.

Een indeling in de **Zvw** vindt plaats als de prestatiecode bij de indicatie voorkomt op een van de volgende codelijsten:
- '065 - Prestatiecodelijst Wijkverpleging'; 
- '066 - Prestatiecodelijst Zorg zintuigelijk gehandicapten'; 
- '068 - Prestatiecodelijst Eerstelijnsverblijf'; 
- '070 - Prestatiecodelijst Geneeskundige Zorg aan Specifieke Patientengroepen';
- '072 - Prestatiecodelijst Wet Zorg en Dwang'.

Een indeling in **Eerstelijnsverblijf (ELV)** vindt plaats als de prestatiecode bij de indicatie voorkomt op de volgende codelijst: 
- '068 - Prestatiecodelijst eerstelijnsverblijf'.

Een indeling in **Geriatrische Revalidatiezorg (GRZ)** vindt plaats als de zorgproductcode bij de indicatie voorkomt op de volgende codelijst: 
- 'Zorgproducten Tabel v20230420' (https://puc.overheid.nl/nza/doc/PUC_740006_22/1/).

Voor de indeling in een **Wlz-sector (VV, VG, LVG, LG, ZGaud, ZGvis, GGZ-B en GGZ-W)** wordt gebruikt gemaakt van zorgprofiel zoals vermeld in het indicatiebesluit. Op basis van de gegevens in het indicatiebesluit wordt bepaald welke declaratie gebruikt dient te worden voor indelingen en/of berekeningen (Zie eerder in deze paragraaf). 

Voor de indeling in een **leveringsvormen (Verblijf, VPT, MPT en PGB)** in de Wlz-sector VV wordt gebruikt gemaakt van de leveringsvorm zoals vermeld in het indicatiebesluit. Op basis van de gegevens in het indicatiebesluit wordt bepaald welke declaratie gebruikt dient te worden voor indelingen en/of berekeningen (Zie eerder in deze paragraaf). 

#### Indelen in Forensische zorg

Een prestatie wordt ingedeeld in **Forensische zorg (FZ)** als de prestatie bij de indicatie voorkomt op de volgende codelijst: 
- Lijst '071- Prestatiecodelijst Geestelijke gezondheidszorg en Forensische zorg volgens ZPM'.

#### Indelen in WMO en Jeugdwet

Een indeling van declaraties in de **Wmo** vindt plaats als de indicatie een WMO-product betreft.

Een indeling van declaraties in de **Jeugdwet** vindt plaats als de indicatie een Jeugdwet product betreft.

**Bronnen (12-2-2024)**
* AW320-bericht: https://www.vektis.nl/standaardisatie/standaarden/AW320-1.4
* Prestatiecodelijsten: https://tog.vektis.nl/WebInfo.aspx?ID=Prestatiecodelijsten
* iWMO: https://informatiemodel.istandaarden.nl/iWmoJw30_Eb10/views/view_28519.html
* COD954 codelijst: https://www.vektis.nl/standaardisatie/codelijsten/COD954-VEKT
* iWMO-codelijst: https://informatiemodel.istandaarden.nl/iWmoJw30_Eb10/views/view_30198.html


### Gebruik van capaciteit en bezetting

Voor de indicatoren die gaan over capaciteit en bezetting geldt het volgende: 

* _Capaciteit_ en _bezetting_ worden niet gecombineerd in één indicator. 
* De capaciteit wordt uitgedrukt in 'aantal wooneenheden' of 'aantal personen' bij een vestiging en/of bij de organisatie als geheel.
* Deze aantallen worden bepaald o.b.v. de registratie in het Elektronisch Cliënten Dossier: aantal woonheden bij een vestiging en per wooneenheid het aantal personen. 
* Het is niet mogelijk om _capaciteit_ uit te drukken in aantallen cliënten of personen met bepaalde kenmerken, zoals diagnose, zorgprofiel, etc. Dit geldt ook voor ‘partners’ van cliënten. 
* Het betreft wooneenheden die geschikt zijn voor bewoning door een of meerdere cliënten en/of een partner. In geval van _capaciteit_ is het dus onbekend of personen cliënten, partners of andere typen personen betreft.
* Een wooneenheid is het samenstel van één, of meerdere kamer of ruimtes (onz-g:Room), die samen geschikt zijn voor bewoning en geen kleinere wooneenheden bevat. Een wooneenheid betreft dus de kleinste eenheid geschikt voor bewoning. Een locatie met meerdere wooneenheden wordt op zichzelf dus niet beschouwd als een wooneenheid, ook al biedt die lokatie wel de mogelijkheid om er te wonen. De nadruk bij dit concept ligt derhalve op het begrip ’–eenheid’.
* De _bezetting_ kan uitgedrukt worden in ‘aantallen wooneenheden waarin cliënten op dat moment verblijven’ of ‘aantallen cliënten met specifieke kenmerken', zoals indicatie, leveringsvorm of zorgprofiel.


### Peildatum, meetmoment en meetperiode

* De peildatum betreft de dag op basis waarvan de indicator berekend wordt.
  
  Bijvoorbeeld: *Het aantal medewerkers met een arbeidsovereenkomst op 1 januari (peildatum) betreft het aantal medewerkers dat op 1 januari beschikte over een arbeidsovereenkomst.*

  NB: De peildatum kan op verschillende manieren bepaald worden. Een peildatum (1 januari 2023) kan bijvoorbeeld als variabele worden ingevoerd, maar ook op basis van de beschikbare gegevens bepaald worden, omdat er geen historie wordt bijgehouden. Dit laatste voorbeeld speelt bij capaciteit en bezetting. De peildatum betreft in dat geval de datum van de datum waarop de gegevens uit het bronsysteem geëxporteerd zijn.

* Het meetmoment betreft het moment waarop een meting wordt gedaan en de indicator of informatievraag wordt berekend met de op dat moment beschikbare gegevens.

  Bijvoorbeeld: *Het aantal medewerkers met een arbeidsovereenkomst op 1 januari (peildatum) kan berekend worden op 1 februari (meetmoment).*

* De meetperiode betreft de periode waarover de indicator of informatievraag wordt berekend.

  Bijvoorbeeld: *Het aantal medewerkers dat in december (meetperiode) in dienst trad kan berekend worden op 1 februari (meetmoment).*

### Populatiegrootte

Bij alle indicatoren Basisveiligheid in het uitwisselprofiel 'Zorginstituut Openbaarmaking kwaliteitsindicatoren verpleeghuiszorg' wordt bij een (deel)resultaat van minder dan 10 personen (bijvoorbeeld cliënten of werknemers; n < 10) de indicator niet berekend. In dat geval wordt het volgende getoond: *'Waarneming kleiner dan 10'*. 

Voor indicatoren 14.1, 14.2, 14.3, 15.4.2 en 16.1 in het uitwisselprofiel 'Zorgkantoren Inkoopondersteuning en beleidsontwikkeling' geldt het volgende: Een (deel)resultaat van minder dan 10 personen (bijvoorbeeld cliënten of werknemers; n < 10) wordt uitsluitend getoond wanneer het getal een totaal voor de organisatie betreft. In geval van andere (deel)resultaten van minder dan 10 personen wordt het volgende getoond: *'Waarneming kleiner dan 10'*. 

Voorbeeld:

Peildatum: 1-1-2023
| Organisatieonderdeel | Totaal | Aantal cliënten met leveringsvorm Verblijf | Aantal cliënten met leveringsvorm VPT  | Aantal cliënten met leveringsvorm MPT |
|---|---|---|---|---| 
| Organisatie | 32 | *Niet van toepassing*  | *Niet van toepassing* | *Niet van toepassing* |
| Vestiging De Beuk | *Niet van toepassing* | *Waarneming kleiner dan 10* | *Waarneming kleiner dan 10* | *Waarneming kleiner dan 10* |
| Vestiging de Eik | *Niet van toepassing*  | 11 | 11 | *Waarneming kleiner dan 10* |
 
Resultaten voordat deze zijn afgeschermd:
| Organisatieonderdeel | Totaal | Aantal cliënten met leveringsvorm Verblijf | Aantal cliënten met leveringsvorm VPT  | Aantal cliënten met leveringsvorm MPT |
|---|---|---|---|---| 
| Organisatie | 32 | 14** | 14** | 4** |
| Vestiging De Beuk | 9** | 3* | 3* | 3* |
| Vestiging de Eik | 23** | 11 | 11 | 1* |

-*- Resultaat wordt afgeschermd omdat de waarneming kleiner dan 10 is.

** Resultaat wordt niet berekend (niet van toepassing).

### Aandachtspunten bij indicatoren die putten uit het grootboek

Voor deze indicatoren geldt het volgende:

1. Het datumveld dat benodigd is bij deze indicatoren is de boekingsdatum. De boekingsdatum is de datum of periode waarop het financiële feit (boeking) betrekking heeft.
2. Bij enkele indicatoren zijn voor de volledigheid hoofd- en subrubrieken opgenomen in de tabellen met de verwijzingen naar de betreffende RGS- en Prismant-rubrieken. Voor het berekenen van de indicator dient elke unieke boeking een keer mee te tellen (en niet dubbel, als zowel, hoofdrubriek als subrubriek zijn beschreven).
3. De beschrijvingen in de gevalideerde vragen gaan steeds uit van de “formele” beschrijvingen in RGS en Prismant. De selectie van de boekingen hangen echter af van een specifieke implementatie bij een zorgaanbieder. Immers, de inrichting van de boekhouding kan afwijken van RGS en Prismant. De zorgaanbieder kan nl. afwijkende grootboekrubrieken hebben gedefinieerd, boekingen onder andere rubrieken hebben ingedeeld of een ander referentieschema gebruiken, etc..
4. Deze indicatoren zijn gespecificeerd op basis van zowel RGS als Prismant. Bij implementatie dient er gekozen te worden voor één van deze twee rubriekenschema’s voor alle indicatoren.
5. Bij Prismant bevatten de rubrieken alleen de boekingen direct op de betreffende rubrieken. Er lijken in Prismant wel "consoliderende" rubrieken te bestaan. Zo betreft rubriek 413000 "(Salarissen en vergoedingen voor) Personeel cliënt - c.q. bewonersgebonden functies" en bevatten de overige rubrieken 413xxx allen kosten die onder deze hoofdnoemer lijken te vallen. Er is echter nergens een eenduidige wijze beschreven hoe rubrieken samengenomen worden. KIK-V gaat er daarom vanuit dat met rubriek 413000 alleen direct op deze rubriek geboekte posten aangeleverd worden, en er geen consolidatie plaatsvindt van de overige 413xxx rubrieken naar 413000.
6. Bij RGS bevatten rubrieken naast de directe boekingen ook de totaaltelling van alle boekingen op alle onderliggende rubrieken. Dus BLim (Liquide middelen) bevat (ook) alle boekingen op BLimKas (Kasmiddelen), BLimBan (Tegoeden bij banken) en BLimKru (Kruisposten). BLimKas (Kasmiddelen) bevat vervolgens (ook) BLimKasKas (Kas) en BLimKasKlk (Kleine kas) etc..
7. Een balans heeft een datum en bevat voor een verzameling balansrubrieken voor iedere rubriek een saldo. Als een balans voor een rubriek geen saldo bevat, dan wordt aangenomen dat het saldo van die rubriek nul is.
8. De startbalans, ook wel openingsbalans genoemd, is de balans bij aanvang van een administratie, nog voordat enige boeking in het grootboek heeft plaatsgevonden.
9. Een beginbalans op een peildatum bevat de startbalans vermeerderd met het totaal van alle (balans)boekingen **tot** de peildatum, dus **tot en met** de dag voor de peildatum.
10. De balans (of eindbalans) op een peildatum bevat de startbalans vermeerderd met alle boekingen **tot en met** de peildatum.
11. Alle indicatoren gaan uit van een maximale resolutie voor zowel de balans als de winst- en verliesrekening van 1 maand. M.a.w. een (eind)balans betreft de laatste dag van een maand, en een periode in de winst- en verliesrekening betreft een geheel aantal maanden. Als gevolg kunnen in het ETL proces van grootboek naar datastation per rubriek alle boekingen per maand geconsolideerd worden. Een dergelijke geconsolideerde boeking dient dan als datum de laatste dag van de betreffende maand te krijgen. Uiteraard mogen de oorspronkelijke boekingen in de betreffende maand dan niet opgenomen worden in het datastation. Dat zou tot dubbeltellingen leiden.
12. In het ETL proces van grootboek naar datastation kunnen daarnaast beginbalansen geïmporteerd worden om de efficientie van berekeningen verder te vergroten. Een dergelijke beginbalans dient steeds als datum de eerste dag van de betreffende maand te hebben, en bevat dus de cumulatieve saldi tot en met de laatste dag van de voorgaande maand. Indicatoren bepalen de balans op een peildatum (altijd de laatste dag van de maand) door het meest recente beginbalans saldo (op de eerste dag van de volgende maand of eerder) te vermeerderen met alle boekingen vanaf die meest recente begindatum tot en met de balansdatum. 
13. Om alle indicatoren te kunnen berekenen dient de vroegste beginbalansdatum niet later dan 1 januari van twee jaar geleden te zijn. In 2025 is dat dus 1 januari 2023. In combinatie met het punt 5 en punt 6 hiervoor is het mogelijk om het datastation te voeden met alleen de beginbalansen op de eerste dag van iedere maand vanaf 1 januari van twee jaar geleden, zonder individuele (geconsolideerde) boekingen.
